""" Modules for molecule testing ansible_role_repositories """

import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("rhel_repos", [("/etc/yum.repos.d/rhel.repo")])
def test_for_rhel_repo_file(host, rhel_repos):
    """
    Test to ensure that the centos test didn't get the rhel repos installed
    """
    rhel = host.file(rhel_repos)
    assert rhel.exists is False


@pytest.mark.parametrize("packages", [("vim-minimal")])
def test_packages(host, packages):
    """
    In the testing there are a couple of packages that are installed to
    verify that the repos are installed and working. This ensures that
    those packages are installed.
    """
    for pkg in packages:
        host.run(f"yum install -y {pkg}")

    package = host.package(packages)

    assert package.is_installed
